#' Table associating subjects and groups in graph_list.
#'
#' @name subjects
#' @docType data
#' @keywords data
NULL

#' Randomly generated connectivity data.
#'
#' @name graph_list
#' @docType data
#' @keywords data
NULL
