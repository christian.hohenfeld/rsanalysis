#' Check for large motion.
#'
#' @param rawdir The directory to search in.
#' @param pattern The pattern to look for.
#' @param parfile (optional) Alternatively you can provide the file directly.
#' @param cutoff A cut-off value for how many mm and degree of motion to accept.
#' @param spike_cutoff Threshold for volume-to-volume sum of motion for that if
#' exceeded labels a volume as spike.
#' @param plot Boolean whether to include a plot of motion parameters.
#'
#' This is designed for FSL par files. The first three colums of the file
#' are the rotation in radians and the last three the translation in mm.
#'
#' @return A list containing: data (tbl_df) containing the raw data,
#'  exceeds (int): the amount of points exceeding the cutoff,
#'  plot (ggplot2): a plot
#'
#' @importFrom rlang .data
#'
#' @export
check_motion <- function(rawdir = NULL, pattern = NULL, parfile = NULL,
                         cutoff = 1, spike_cutoff = 0.5, plot = FALSE) {
  stopifnot(!all(is.null(c(rawdir, pattern, parfile))))
  if (is.null(parfile)) {
    motion_file <- list.files(rawdir, pattern = pattern, full.names = TRUE)
    if (length(motion_file) > 1) {
      stop("Found more than one file, this is not supported yet.")
    }
  } else {
    motion_file <- parfile
  }

  coltypes <- readr::cols(
    rot_x = readr::col_double(),
    rot_y = readr::col_double(),
    rot_z = readr::col_double(),
    tra_x = readr::col_double(),
    tra_y = readr::col_double(),
    tra_z = readr::col_double()
  )

  motion_cols <- c("rot_x", "rot_y", "rot_z", "tra_x", "tra_y", "tra_z")
  motion_data <- readr::read_table(
    motion_file,
    col_names = motion_cols,
    col_types = coltypes)

  rad_to_deg <- function(x) {
    x * 180 / pi
  }

  motion_data <- motion_data |>
    dplyr::mutate(
      rot_x = rad_to_deg(.data$rot_x),
      rot_y = rad_to_deg(.data$rot_y),
      rot_z = rad_to_deg(.data$rot_z)
    )


  spikes <- apply(motion_data, 2, cdiff)
  spikes[, 1:3] <- 100 * pi * spikes[, 1:3] / 360
  has_spike <- rowSums(abs(spikes)) > spike_cutoff

  motion_data <- motion_data |>
    dplyr::mutate(
      spike = has_spike,
      exceeds_cutoff = apply(motion_data, 1, function(x) any(abs(x) > cutoff))
    )

  alldata <- list(
    data = motion_data,
    exceeds_motion = sum(motion_data[["exceeds_cutoff"]]),
    exceeds_spike = sum(motion_data[["spike"]]),
    flag_motion = which(motion_data[["exceeds_cutoff"]]),
    flag_spike = which(motion_data[["spike"]])
  )

  if (plot) {
    exc_sym <- rlang::sym("exceeds_cutoff")
    spk_sym <- rlang::sym("spike")
    vol_sym <- rlang::sym("vol")
    value_sym <- rlang::sym("value")
    name_sym <- rlang::sym("name")

    motion_data <- motion_data |>
      dplyr::mutate(vol = seq_len(nrow(motion_data)))
    motion_long <- motion_data |> tidyr::pivot_longer(
      cols = c(-{{ exc_sym }}, -{{ vol_sym }}, -{{ spk_sym }}))
    motion_plot <- ggplot2::ggplot(motion_long) +
      ggplot2::aes(x = {{ vol_sym }}, y = {{ value_sym }},
                   group = {{ name_sym }}, colour = {{ name_sym }}) +
      ggplot2::geom_hline(yintercept = c(cutoff, cutoff * - 1),
                          colour = "blue4") +
      ggplot2::geom_line() +
      ggplot2::geom_point() +
      ggthemes::scale_colour_colorblind() +
      ggplot2::labs(x = "Volume", y = "Rotation deg/Translation mm",
                    colour = "Dimension") +
      ggplot2::theme_minimal()

    alldata[["plot"]] <- motion_plot
  }

  alldata
}
