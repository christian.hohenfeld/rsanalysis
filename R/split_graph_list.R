#' Split a list of graphs into tbl_dfs by group.
#'
#' @param datalist A named list of graphs.
#' @param subjects A data.frame associating subjects and groups, subjects must
#' correspond to names of datalist.
#'
#' @return A list of tbl dfs.
#'
#' @importFrom rlang .data
#'
#' @export
split_graph_list <- function(datalist, subjects) {
  subjects <- subjects %>%
    dplyr::filter(.data$id %in% names(datalist))

  groups <- subjects %>%
    dplyr::group_by(.data$group) %>%
    dplyr::group_keys()

  group_ids <- lapply(subjects %>%
    dplyr::group_by(.data$group) %>%
    dplyr::group_split(), function(x) x$id)

  names(group_ids) <- groups$group
  split_data <- lapply(seq_along(group_ids), function(x) {
    datalist[names(datalist) %in% group_ids[[x]]]
  })

  split_data <- lapply(seq_along(split_data), function(x) {
    lapply(seq_along(split_data[[x]]), function(y) {
        split_data[[x]][[y]] %>%
             tidygraph::activate("nodes") %>%
             dplyr::mutate(id = group_ids[[x]][y])
        })
  })

  split_tbls <- lapply(split_data, function(x) {
      lapply(x, function(y) {
        y %>%  tidygraph::activate("nodes") %>% tibble::as_tibble()
      })
  })
  split_tbls <- lapply(split_tbls, function(x) do.call("rbind", x))
  names(split_tbls) <- groups$group

  split_tbls
}
