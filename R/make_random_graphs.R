#' Make a list of random graphs.
#'
#' @param reference_graphs (list) A list of tbl_graph objects used to for
#' determining the size of the random graphs.
#' @param n (int) The amount of graphs to generate.
#' @param calc_mean_dist (logical) If TRUE calculates mean_distance for all
#' graphs.
#'
#' @return A list of tbl_graph objects of length `n`.
#' @export
make_random_graphs <- function(reference_graphs, n, calc_mean_dist = FALSE) {
    degree <- vapply(reference_graphs, function(x) mean(igraph::degree(x)),
                     numeric(1), USE.NAMES = FALSE)
    degree <- round(mean(degree), 0)
    order <- vapply(reference_graphs, igraph::gorder,
                    numeric(1), USE.NAMES = FALSE)
    order <- round(mean(order), 0)
    # use https://igraph.org/r/doc/sample_k_regular.html

    random_list <- lapply(1:n, function(x) {
        igraph::sample_k_regular(
            no.of.nodes = order,
            k = degree,
            directed = FALSE,
            multiple = FALSE
        ) %>% tidygraph::as_tbl_graph()
    })

    if (calc_mean_dist) {
        random_list <- lapply(
            seq_along(random_list), function(x) {
                random_list[[x]] %>%
                    dplyr::mutate(
                        mean_dist = igraph::mean_distance(random_list[[x]]))
            })
    }
    rand_names <- paste0("rand", sprintf("%04d", c(1:n)))
    names(random_list) <- rand_names
    random_list
}
