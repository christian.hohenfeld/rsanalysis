#' Get measures grouped by node names.
#'
#' @param datalist The data to work on.
#' @param subjects A table identifying subjects and groups.
#' @param name The varibale to group by.
#' @param measure The measure to find data for.
#'
#' @return A summary table.
#'
#' @importFrom rlang .data
#'
#' @export
node_measures <- function(datalist, subjects, name, measure) {
  split_tbl <- split_graph_list(datalist, subjects)
  by_name <- lapply(split_tbl, function(x) {
      x %>%
        dplyr::group_by({{ name }}) %>%
        dplyr::summarise(
            "{{ measure }}_mean" := mean({{ measure }}), # nolint
            "{{ measure }}_sd" := stats::sd({{ measure }})) # nolint
     })
  by_name
}
