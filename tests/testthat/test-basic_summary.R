describe("basic_summary", {
    data("graph_list")
    data("subjects")

    graph_list <- apply_measure(graph_list, "nodes", "degree",
                                tidygraph::centrality_degree)

    it("generates a list with two items if no measure is specified", {
        by_group <- basic_summary(graph_list, subjects, id, group)
        expect_length(by_group, 2)
    })

    it("generates a list with summary and nested list if measure is given", {
        with_measure <- basic_summary(graph_list, subjects, id, group, degree)
        expect_length(with_measure, 2)
        expect_length(with_measure$raw, 2)
    })

    it("groups data by group from subjects table", {
        groups <- unique(subjects$group)
        with_measure <- basic_summary(graph_list, subjects, id, group, degree)
        expect_true(all(groups %in% with_measure$summary$group))
        expect_true(all(groups %in% names(with_measure$raw)))
    })
})
