# Changelog

## Version 0.3, 2021-07-08

* Add label_means function to extract mean coordinates from atlas files

## Version 0.2, 2021-04-27

* Merge most helpers from other projects into this main package
* Add QA stuff like tests and linting
* Improve documentation
