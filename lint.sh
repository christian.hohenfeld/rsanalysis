#! /usr/bin/env Rscript
devtools::load_all(".")
lint_out <- lintr::lint_package()
print(lint_out)
quit(save = 'no', status = length(lint_out))
